# cveil
cveil (pronounced Sea Veil) is an experimental use of the public Veilid Network for cybersecurity information sharing. For example, there is public access for certain updates to subkey values associated with CVEs.

In this 0.0.2-alpha version, there is one DHT record for copies of the current year's CVEs (the ones with CVE-2023) and a second DHT record for Veilid users to provide some basic status updates for these CVEs. The latter is slightly more interesting. Writer information is publicly available, but the usual DHTSchema::SMPL validation is extended (only on the node that originally did create_dht_record for these) to have a much smaller vocabulary of values. In particular, Veilid users can provide information about "missing" CVEs, i.e., ones that have gone public but can't be found on CVE's websites. The current vocabulary is, in order of precedence:

- supplier (the CVE was disclosed by the supplier of the software or hardware)
- discoverer (the CVE was disclosed by whoever discovered the security problem)
- search (the CVE can be found with web searches such as Google)
- other (none of the above)
- CVE-nnnn-n* (new CVE is superfluous because there's already a CVE for the same bug)

If there's an attempt to set a subkey value outside of this vocabulary, the set_dht_value outcome is ``error=Generic: failed schema validation`` but, within the vocabulary, the [usual behavior](https://gitlab.com/veilid/veilid/-/blob/513116e672b39f340ee3ae7e75e1af90913c0e7d/veilid-core/src/veilid_api/routing_context.rs#L257-L258) occurs:

- Returns None if the value was successfully put
- Returns Some(data) if the value put was older than the one available on the network

There's the obvious mapping to 2023 CVEs, starting from subkey 1 for CVE-2023-0001 and allowing up to subkey 65534 for CVE-2023-65534. Subkey 0 is for metadata, e.g., other keys that were used once the first key (which is ``VmaiHawidTJCc7LC26ynO9F06w046MYj5UFjAHSZzVA``) reached the maximum allowed size (1048576 bytes). There could also be metadata about another DHT record for the 16-bit overflow if the numbers went past CVE-2023-65534, but this isn't currently likely. If CVE-2022 is added, that would be its own DHT record (subkey 1 for CVE-2022-0001 etc.). Not all of the CVE-2023 ones have been sent to the Veilid Network (and, even if they were, they can go away with Veilid server LRU eviction).

Mnemonic: the first four letters ``Vmai`` refer to "vulnerability metadata and information"

The DHT record for the public set_dht_value calls is ``tuUtTBWXD00GlGAPZ0gkiTcM8G1YrVFAwAWSlywM8sc`` with ``qL4y5lXbJ-dS8rmdW2DX0mYjrlTloUIGkYdStVJu8Qs`` and ``qVudCJsc4WbJFlVud_GlEcV1I8PL3Uh86LnVfJPcOg8`` for access. Expectation is that this gives write access to all of the subkeys but if it allows other access (change the DHT record attributes) then maybe that's the first CVE filed against cveil.

Mnemonic: the first four letters ``tuUt`` are similar to terms used elsewhere for user-generated content ("tweet" was used by Twitter until July 2023; "toot" was used by Mastodon until November 2022)

To make the set_dht_value calls, an application may need several steps; here's an outline of one way that works in Rust (maybe not the simplest way):
1. decode the base64
1. convert to [u8; 32]
1. call CryptoKey::new
1. (first three steps for both the writer and the writer's secret)
1. call KeyPair::new
1. base64 decoding and [u8; 32] conversion of the DHT record's key
1. TypedKey::new for that key
1. call open_dht_record with that key and the keypair
1. call set_dht_value with the key, subkey number, and an allowed value (e.g., "supplier" as a Vec)

## veilid-file compatibility
Data at subkey 0 can be retrieved with the standard version of [veilid-file](https://gitlab.com/vatueil/veilid-file), e.g.,

>>>
poetry run file get VLD0:VmaiHawidTJCc7LC26ynO9F06w046MYj5UFjAHSZzVA
>>>

but if you want to look up arbitrary CVEs (or anything in ``tuUtTBWXD00GlGAPZ0gkiTcM8G1YrVFAwAWSlywM8sc``) you would need to arrange for a different value of chunk_num to end up in the [get_dht_value call](https://gitlab.com/vatueil/veilid-file/-/blob/4a888d809e583a71723bd9a13dd0dc37af3856a2/veilid_file/file.py#L79).

## FAQ

- What happens to my subkey value? There are two possibilities. First, the value might be sent to one of the few Veilid nodes that belong to cveil. This has been seen during testing but is infrequent (less than 10% chance). This presumably depends on fanout and other factors, and might be more frequent depending on future Veilid development. Second, almost always (during testing) the subkey value could be found by other nodes that actively ask for it, i.e., open_dht_record and get_dht_value. So, for example, if someone working on cveil wanted to know if anyone posted information about CVE-2023-54321, they could call get_dht_value for subkey number 54321 (of ``tuUtTBWXD00GlGAPZ0gkiTcM8G1YrVFAwAWSlywM8sc``). Obviously this isn't a perfect situation, it's just a 0.0.2-alpha attempt at using Veilid for a new use case.

- Why have a limited vocabulary? To avoid any unwanted content within ``tuUtTBWXD00GlGAPZ0gkiTcM8G1YrVFAwAWSlywM8sc`` (spam, etc.). In the future, there might be a way to remove the vocabulary restrictions for a subset of Veilid users.

- Why is a privacy technology like Veilid relevant to this use case? First, maybe you want Veilid for all possible use cases to avoid tracking. Second, there are sometimes different opinions on whether a CVE has gone public (it's one person's intended disclosure but another person's leak).

- Why is a privacy technology relevant to ``VmaiHawidTJCc7LC26ynO9F06w046MYj5UFjAHSZzVA`` (just the CVEs, not the updates)? Again, to avoid tracking. Also, maybe you want to look up one CVE without anyone knowing who's looking (it can mean that you're using whatever is vulnerable to the CVE and haven't patched yet).

- Can't anyone just overwrite a useful/correct subkey value as soon as they see it? Yes, but the first nodes to see it might have already re-published it in Veilid with better access control, or published it outside of Veilid. In the future, it may be possible to further adjust the ``tuUtTBWXD00GlGAPZ0gkiTcM8G1YrVFAwAWSlywM8sc`` subkey behavior (e.g., both limited vocabulary and write-once).

- Can't anyone just flood the set of subkeys with wrong information? Yes, and if this happens often, cveil would need to stop granting authorization to the general public.

- Does this make the network inconsistent, i.e., one node would be asserting ``failed schema validation`` but others would not? Possibly; there might be a workaround if Veilid later adds support for user-defined schemas.

- Are all CVEs small enough for the 32KB size limit of a subkey? No, it might only be around 95% in 2023. Some parts of CVEs (the affected and references keys) may have short replacement values when necessary to stay below the 32768 cutoff.

- Are there other downsides of cveil (performance impact on the Veilid Network, usability problems, design flaws, etc.)? None are known at the moment.
